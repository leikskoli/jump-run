extends Node
class_name Main

@onready var player = $Player
@onready var tunnel = $Tunnel

var rotation_speed: float = 0.25
var current_angle: float = 0.0
var target_angle: float = 0.0
var current_lerp: float = 1.0

func _physics_process(delta):
	if current_lerp >= 1.0:
		if Input.is_action_just_pressed("rotate_cw"):
			target_angle -= 45.0
			current_lerp = 0
		if Input.is_action_just_pressed("rotate_ccw"):
			target_angle += 45.0
			current_lerp = 0
	
	if current_lerp < 1.0:
		current_lerp += delta / rotation_speed
		tunnel.rotation_degrees.z = lerp(current_angle, target_angle, current_lerp)
		print(current_angle, " / ", target_angle)
	else:
		current_angle = target_angle
