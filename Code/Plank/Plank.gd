@tool
extends StaticBody3D
class_name Plank

@onready var mesh_instance_3d = $MeshInstance3D
@export var new_material: StandardMaterial3D:
	set(value):
		new_material = value
		if is_inside_tree():
			mesh_instance_3d.material_override = new_material
			

func _ready():
	new_material = new_material
