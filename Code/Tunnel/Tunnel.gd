extends Node3D
class_name Tunnel

var speed: float = 7.0


func _process(delta):
	position.z += speed * delta
