extends CharacterBody3D
class_name Player

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var speed: float = 2.5
var jump_velocity: float = 5.0
var spawning_point: Vector3


func _ready():
	spawning_point = position


func _physics_process(delta):
	# Respawn after falling too far.
	if position.y < -100:
		position = spawning_point
	
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Handle jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = jump_velocity

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("left", "right", "up", "down")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * speed
		velocity.z = direction.z * speed
	else:
		velocity.x = move_toward(velocity.x, 0, speed)
		velocity.z = move_toward(velocity.z, 0, speed)
	
	# Find the angle of the Plank we hit.
	#var collision = move_and_collide(velocity * delta)
	#if collision:
		#print(collision)
		#velocity = velocity.slide(collision.get_normal())
	
	move_and_slide()

